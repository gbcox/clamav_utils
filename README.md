Utilities for ClamAV
================================

Various utilities and configuration samples to assist with use and
management of ClamAV scans.

Additional information concerning listed utilities and sample configurations
can be found at The Shiny Object Blog:  http://tso.bzb.us

Licensed under GNU GPL version 3

Copyright © 2017 Gerald Cox (unless explicitly stated otherwise)
